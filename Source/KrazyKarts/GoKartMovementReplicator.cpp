#include "GoKartMovementReplicator.h"
#include "GoKartMovementComponent.h"
#include "Net/UnrealNetwork.h"
#include <GameFramework/GameStateBase.h>
#include "Engine/EngineTypes.h"

// Sets default values for this component's properties
UGoKartMovementReplicator::UGoKartMovementReplicator() {
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicated(true);
}

auto UGoKartMovementReplicator::UpdateServerState(const FGoKartMove& Move) -> void {
	if (MovementComponent == nullptr) {
		UE_LOG(LogTemp, Error, TEXT("Movement component BUSTED"));
		return;
	}

	ServerState.LastMove = Move;
	ServerState.Transform = GetOwner()->GetActorTransform();
	ServerState.Velocity = MovementComponent->GetVelocity();
}

void UGoKartMovementReplicator::CreateSpline(FHermiteCubicSpline& Spline) {
	const float VelocityToDerivative = ClientTimeBetweenLastUpdates * 100;
	Spline.TargetLocation = ServerState.Transform.GetLocation();
	Spline.StartLocation = ClientStartTransform.GetLocation();
	Spline.StartDerivative = ClientStartVelocity * VelocityToDerivative;
	Spline.TargetDerivative = ServerState.Velocity * VelocityToDerivative;
}

void UGoKartMovementReplicator::SetLocation(FHermiteCubicSpline Spline, const float LerpRatio) const {
	const FVector NewLocation = Spline.InterpolateLocation(LerpRatio);
	if (MeshOffsetRoot != nullptr) {
		MeshOffsetRoot->SetWorldLocation(NewLocation);
	}
}

void UGoKartMovementReplicator::SetVelocity(FHermiteCubicSpline Spline, const float LerpRatio) const {
	const float VelocityToDerivative = ClientTimeBetweenLastUpdates * 100;
	const FVector NewDerivative = Spline.InterpolateDerivative(LerpRatio);
	const FVector NewVelocity = NewDerivative / VelocityToDerivative;
	MovementComponent->SetVelocity(NewVelocity);
}

void UGoKartMovementReplicator::SetRotation(const float LerpRatio) const {
	const FQuat TargetRotation = ServerState.Transform.GetRotation();
	const FQuat StartRotation = ClientStartTransform.GetRotation();
	const auto NextRotation = FQuat::Slerp(StartRotation, TargetRotation, LerpRatio);
	if (MeshOffsetRoot != nullptr) {
		MeshOffsetRoot->SetWorldRotation(NextRotation);
	}
}

auto UGoKartMovementReplicator::ClientTick(float DeltaTime) -> void {
	ClientTimeSinceUpdate += DeltaTime;

	if (ClientTimeBetweenLastUpdates < KINDA_SMALL_NUMBER) return;
	if (MovementComponent == nullptr) {
		UE_LOG(LogTemp, Error, TEXT("Movement component BUSTED"));
		return;
	}

	const float LerpRatio = ClientTimeSinceUpdate / ClientTimeBetweenLastUpdates;

	FHermiteCubicSpline Spline;
	CreateSpline(Spline);
	SetLocation(Spline, LerpRatio);
	SetVelocity(Spline, LerpRatio);
	SetRotation(LerpRatio);
}

// Called when the game starts
void UGoKartMovementReplicator::BeginPlay() {
	Super::BeginPlay();

	MovementComponent = GetOwner()->FindComponentByClass<UGoKartMovementComponent>();
}


// Called every frame
void UGoKartMovementReplicator::TickComponent(float DeltaTime, ELevelTick TickType,
                                              FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (MovementComponent == nullptr) {
		UE_LOG(LogTemp, Error, TEXT("Movement component BUSTED"));
		return;
	}

	const auto LastMove = MovementComponent->GetLastMove();

	// We are a client in control of the pawn
	if (GetOwnerRole() == ROLE_AutonomousProxy) {
		UnacknowledgedMoves.Add(LastMove);
		Server_SendMove(LastMove);
	}

	// We are the server and in control of the pawn
	if (GetOwner()->GetRemoteRole() == ROLE_SimulatedProxy) {
		UpdateServerState(LastMove);
	}

	// We are a pawn from another client, not in control
	if (GetOwnerRole() == ROLE_SimulatedProxy) {
		ClientTick(DeltaTime);
	}
}

void UGoKartMovementReplicator::OnRep_ServerState() {
	if (GetOwnerRole() == ROLE_AutonomousProxy) {
		AutonomousProxy_OnRep_ServerState();
	}

	if (GetOwnerRole() == ROLE_SimulatedProxy) {
		SimulatedProxy_OnRep_ServerState();
	}
}

void UGoKartMovementReplicator::AutonomousProxy_OnRep_ServerState() {
	if (MovementComponent == nullptr) {
		UE_LOG(LogTemp, Error, TEXT("Movement component BUSTED"));
		return;
	}

	GetOwner()->SetActorTransform(ServerState.Transform);
	MovementComponent->SetVelocity(ServerState.Velocity);

	ClearAcknowledgedMoves(ServerState.LastMove);

	for (auto Move : UnacknowledgedMoves) {
		MovementComponent->SimulateMove(Move);
	}
}

void UGoKartMovementReplicator::SimulatedProxy_OnRep_ServerState() {
	if (MovementComponent == nullptr) {
		UE_LOG(LogTemp, Error, TEXT("Movement component BUSTED"));
		return;
	}

	ClientTimeBetweenLastUpdates = ClientTimeSinceUpdate;
	ClientTimeSinceUpdate = 0;

	if (MeshOffsetRoot != nullptr) {
		ClientStartTransform.SetLocation(MeshOffsetRoot->GetComponentLocation());
		ClientStartTransform.SetRotation(MeshOffsetRoot->GetComponentQuat());
	}
	ClientStartVelocity = MovementComponent->GetVelocity();

	GetOwner()->SetActorTransform(ServerState.Transform);
}

void UGoKartMovementReplicator::Server_SendMove_Implementation(FGoKartMove Move) {
	if (MovementComponent == nullptr) {
		UE_LOG(LogTemp, Error, TEXT("Movement component BUSTED"));
		return;
	}

	CientSimulatedTime += Move.DeltaTime;

	MovementComponent->SimulateMove(Move);

	UpdateServerState(Move);
}

bool UGoKartMovementReplicator::Server_SendMove_Validate(FGoKartMove Move) {
	float ProposedTime = CientSimulatedTime + Move.DeltaTime;
	if ( ProposedTime < GetWorld()->TimeSeconds) {
		UE_LOG(LogTemp, Error, TEXT("Client running too fast"));
		return false;
	}
	if (!Move.IsValid()) {
		UE_LOG(LogTemp, Error, TEXT("Received invalid move"));
		return false;
	}
	return true;
}

void UGoKartMovementReplicator::ClearAcknowledgedMoves(const FGoKartMove& LastMove) {
	TArray<FGoKartMove> NewMoves;

	for (const FGoKartMove& Move : UnacknowledgedMoves) {
		if (Move.Time > LastMove.Time) NewMoves.Add(Move);
	}
	UnacknowledgedMoves = NewMoves;
}

void UGoKartMovementReplicator::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UGoKartMovementReplicator, ServerState);
}
