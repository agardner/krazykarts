#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GoKartMovementComponent.generated.h"

USTRUCT()
struct FGoKartMove {
	GENERATED_BODY()

	UPROPERTY()
	float Throttle;

	UPROPERTY()
	float SteeringThrow;

	UPROPERTY()
	float DeltaTime;

	UPROPERTY()
	float Time;

	bool IsValid() const {
		return FMath::Abs(Throttle) <= 1 && FMath::Abs(SteeringThrow);
	}
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class KRAZYKARTS_API UGoKartMovementComponent : public UActorComponent {

	GENERATED_BODY()

public:

	// Sets default values for this component's properties
	UGoKartMovementComponent();

protected:

	// Called when the game starts
	virtual void BeginPlay() override;

public:

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;

	void SetThrottle(float AxisValue) { Throttle = AxisValue; }

	void SetSteeringThrow(float AxisValue) { SteeringThrow = AxisValue; }

	void SimulateMove(const FGoKartMove& Move);

	void SetVelocity(const FVector& Val) { Velocity = Val; }

	FVector GetVelocity() const { return Velocity; }

	FGoKartMove GetLastMove() const { return LastMove; }
private:

	// The mass of the car
	UPROPERTY(EditAnywhere)
	float Mass = 1000;

	// Minimum radius of the car turning circle at full lock (m)
	UPROPERTY(EditAnywhere)
	float MinTurningCircleRadius = 10;

	// Used to calculate air resistance, changes on a car
	UPROPERTY(EditAnywhere)
	float DragCoefficient = 8;

	// Force applied to the car when the throttle is fully engaged
	UPROPERTY(EditAnywhere)
	float MaxDrivingForce = 10000;

	// Force needed to push a wheeled vehicle forward per unit force of weight
	UPROPERTY(EditAnywhere)
	float RollingResistanceCoefficient = 0.010;

	float SteeringThrow;

	float Throttle;

	FVector Velocity;

	FGoKartMove LastMove;

	FGoKartMove CreateMove(float DeltaTime) const;

	FVector GetAirResistance() const;

	FVector GetRollingResistance() const;

	void UpdateLocationFromVelocity(float DeltaTime);

	auto ApplyRotation(const float DeltaTime, const float AppliedSteeringThrow) -> void;
};
