// Fill out your copyright notice in the Description page of Project Settings.


#include "GoKartMovementComponent.h"
#include "Components/ActorComponent.h"
#include <GameFramework/GameStateBase.h>

const int32 GMeters_To_Centimeters_Conversion_Product = 100;

// Sets default values for this component's properties
UGoKartMovementComponent::UGoKartMovementComponent() {
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UGoKartMovementComponent::BeginPlay() {
	Super::BeginPlay();
}

// Called every frame
void UGoKartMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                             FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// We are in control of the pawn as a client or the server
	if (GetOwnerRole() == ROLE_AutonomousProxy || GetOwner()->GetRemoteRole() == ROLE_SimulatedProxy) {
		LastMove = CreateMove(DeltaTime);
		SimulateMove(LastMove);
	}
}

void UGoKartMovementComponent::SimulateMove(const FGoKartMove& Move) {
	FVector Force = GetOwner()->GetActorForwardVector() * MaxDrivingForce * Move.Throttle;

	Force += GetAirResistance();
	Force += GetRollingResistance();

	const FVector Acceleration = Force / Mass;

	Velocity += Acceleration * Move.DeltaTime;

	ApplyRotation(Move.DeltaTime, Move.SteeringThrow);

	UpdateLocationFromVelocity(Move.DeltaTime);
}

FGoKartMove UGoKartMovementComponent::CreateMove(const float DeltaTime) const {
	return FGoKartMove{Throttle, SteeringThrow, DeltaTime, GetWorld()->TimeSeconds};
}

FVector UGoKartMovementComponent::GetRollingResistance() const {
	const auto AccelerationGravity = -GetWorld()->GetGravityZ() / 100;
	const auto NormalForce = Mass * AccelerationGravity;
	return -1 * Velocity.GetSafeNormal() * RollingResistanceCoefficient * NormalForce;
}

FVector UGoKartMovementComponent::GetAirResistance() const {
	return -1 * Velocity.SizeSquared() * DragCoefficient * Velocity.GetSafeNormal();
}

auto UGoKartMovementComponent::ApplyRotation(const float DeltaTime, const float AppliedSteeringThrow) -> void {
	const float DeltaLocation = FVector::DotProduct(GetOwner()->GetActorForwardVector(), Velocity) * DeltaTime;
	const float RotationAngle = DeltaLocation / MinTurningCircleRadius * AppliedSteeringThrow;
	const FQuat RotationDelta(GetOwner()->GetActorUpVector(), RotationAngle);

	Velocity = RotationDelta.RotateVector(Velocity);

	GetOwner()->AddActorWorldRotation(RotationDelta);
}

void UGoKartMovementComponent::UpdateLocationFromVelocity(const float DeltaTime) {
	// Translating from meters to centimeters
	const FVector Translation = Velocity * 100 * DeltaTime;

	FHitResult Hit;
	GetOwner()->AddActorWorldOffset(Translation, true, &Hit);
	if (Hit.IsValidBlockingHit())
		Velocity = FVector::ZeroVector;
}
